﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;

namespace CallbackContract
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class CallbackCalculator : ICallbackCalculator
    {
        double result;
        ICallbackHandler callbackHandler = null;

        public CallbackCalculator()
        {
            callbackHandler = OperationContext.Current.GetCallbackChannel<ICallbackHandler>();
        }

        public void ObliczCos(int sek)
        {
            Console.WriteLine("...wywolano ObliczCos({0})", sek);
            if(sek < 10)
            {
                Thread.Sleep(sek * 1000);
            } else
            {
                Thread.Sleep(1000);
            }
            callbackHandler.ZwrotObliczCos("Obliczenia trwaly " + (sek + 1) + " sekund(y)");
        }

        public void PrzeslijTekst(string tekst)
        {
            Console.WriteLine("Przesłano tekst: {0}", tekst);
        }

        public void Silnia(double n)
        {
            Console.WriteLine("...wywolano Silnia({0})", n);
            Thread.Sleep(3000);
            result = 1;
            for(int i = 1; i <= n; i++)
            {
                result *= i;
            }
            callbackHandler.ZwrotSilnia(result);
        }


    }
}
