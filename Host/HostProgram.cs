using CallbackContract;
using Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace Host
{
    class HostProgram
    {
        static void Main(string[] args)
        {
            ServiceHost host = new ServiceHost(typeof(MyService));
            ServiceHost host2 = new ServiceHost(typeof(CallbackCalculator));
            try
            {
                ServiceEndpoint endpoint = host.Description.Endpoints.Find(typeof(IService));
                Console.WriteLine("Service endpoint {0}:", endpoint.Name);
                Console.WriteLine("Binding {0}:", endpoint.Binding.ToString());
                Console.WriteLine("ListenUri {0}:", endpoint.ListenUri.ToString());
                host.Open();
                
                Console.WriteLine("Service 1 is running...");

                ServiceEndpoint endpoint2 = host2.Description.Endpoints.Find(typeof(ICallbackCalculator));
                Console.WriteLine("Service endpoint {0}:", endpoint2.Name);
                Console.WriteLine("Binding {0}:", endpoint2.Binding.ToString());
                Console.WriteLine("ListenUri {0}:", endpoint2.ListenUri.ToString());
                host2.Open();

                Console.WriteLine("Service 2 is running...");

                Console.ReadKey();
                host.Close();
                host2.Close();
            }
            catch (CommunicationException e)
            {
                Console.WriteLine("An error occurred: {0}", e.Message);
                host.Abort();
                host2.Abort();
            }
        }
    }
}
