using CallbackContract;
using StreamingContract;
using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace StreamingHost
{
    class StreamingHostProgram
    {
        static void Main(string[] args)
        {
            Uri baseAddress = new Uri("http://localhost:44667/");
            Uri secondAddress = new Uri("http://localhost:44668/");
            ServiceHost host = new ServiceHost(typeof(StreamingService), baseAddress);
            ServiceHost host2 = new ServiceHost(typeof(CallbackCalculator), secondAddress);

            try
            {
                BasicHttpBinding binding = new BasicHttpBinding();
                binding.TransferMode = TransferMode.Streamed;
                binding.MaxReceivedMessageSize = 1000000000000000000;
                binding.MaxBufferSize = 8192;

                ServiceEndpoint endpoint = host.AddServiceEndpoint(typeof(IStreamingService), binding, baseAddress);
                
                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;
                host.Description.Behaviors.Add(smb);
                host.Open();


                WSDualHttpBinding binding2 = new WSDualHttpBinding();
                ServiceEndpoint endpoint2 = host2.AddServiceEndpoint(typeof(ICallbackCalculator), binding2, secondAddress);
                ServiceMetadataBehavior smb2 = new ServiceMetadataBehavior();
                smb2.HttpGetEnabled = true;
                host2.Description.Behaviors.Add(smb2);
                host2.Open();

                Console.WriteLine("Host running...");
                Console.ReadKey();
                host.Close();
            } catch(CommunicationException e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
                host.Abort();
            }
        }
    }
}
