﻿using System;
using Client.ServiceReference2;

namespace Client
{
    class CallbackHandler : ICallbackCalculatorCallback
    {
        public void ZwrotObliczCos(string result)
        {
            Console.WriteLine("Obliczenia: {0}", result);
        }

        public void ZwrotSilnia(double result)
        {
            Console.WriteLine("Silnia = {0}", result);
        }
    }
}
