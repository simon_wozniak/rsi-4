using Client.ServiceReference1;
using Client.ServiceReference2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Client
{
    class ClientProgram
    {
        static void Main(string[] args)
        {
            ServiceClient client = new ServiceClient("WSHttpBinding_IService");
            Console.WriteLine("...wywoluje funkcja 1:");
            client.Funkcja1("Klient1");
            Thread.Sleep(10);

            Console.WriteLine("...kontynuacja po funkcji 1:");
            Console.WriteLine("...wywoluje funkcja 2:");
            client.Funkcja2("Klient1");
            Thread.Sleep(10);
            
            Console.WriteLine("...kontynuacja po funkcji 2:");
            Console.WriteLine("...wywoluje funkcja 1:");
            client.Funkcja1("Klient1");
            Thread.Sleep(10);
            Console.WriteLine("...kontynuacja po funkcji 1:");
            client.Close();
            Console.WriteLine("KONIEC KLIENT1");

            Console.WriteLine("\nKLIENT 2: ");
            CallbackHandler callbackHandler = new CallbackHandler();
            InstanceContext instanceContext = new InstanceContext(callbackHandler);
            CallbackCalculatorClient client2 = new CallbackCalculatorClient(instanceContext);
            double value1 = 10;
            Console.WriteLine("...wywoluje Silnia({0})...", value1);
            client2.Silnia(value1);
                                    
            value1 = 20;
            Console.WriteLine("...wywoluje Silnia({0})...", value1);
            client2.Silnia(value1);

            int value2 = 2;
            Console.WriteLine("...wywoluje obliczenia cosia...", value1);
            client2.ObliczCos(value2);
            Console.WriteLine("...czekam chwile na odbior wynikow");
            Thread.Sleep(5000);

            client2.Close();
            Console.WriteLine("KONIEC KLIENT2");
        }
    }
}
