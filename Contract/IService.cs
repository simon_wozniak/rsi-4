﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Contract
{
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        void Funkcja1(string s1);

        [OperationContract(IsOneWay = true)]
        void Funkcja2(string s2);
    }
}
