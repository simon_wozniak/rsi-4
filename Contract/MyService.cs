﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;

namespace Contract
{
    [ServiceBehavior(ConcurrencyMode=ConcurrencyMode.Multiple)]
    public class MyService : IService
    {
        public void Funkcja1(string s1)
        {
            Console.WriteLine("...{0}: funkcja1 - start", s1);
            Thread.Sleep(3000);
            Console.WriteLine("...{0}: funkcja1 - stop", s1);
        }

        public void Funkcja2(string s2)
        {
            Console.WriteLine("...{0}: funkcja2 - start", s2);
            Thread.Sleep(3000);
            Console.WriteLine("...{0}: funkcja2 - stop", s2);
        }
    }
}
