using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using StreamingClient.ServiceReference1;
using System.IO;

namespace StreamingClient
{
    class StreamingClientProgram
    {
        static void Main(string[] args)
        {
            StreamingServiceClient client = new StreamingServiceClient();
            string filePath = Path.Combine(System.Environment.CurrentDirectory, "klient.jpg");

            Console.WriteLine("Wywoluje getStream()");
            Stream stream = client.GetStream("image.jpg");
            ZapiszPlik(stream, filePath);

            //
            Console.WriteLine("Wywoluje getMStream()");
            Stream fs = null;
            long rozmiar;
            string nnn = "image.jpg";
            rozmiar = client.GetMStream(ref nnn, out fs);
            filePath = Path.Combine(System.Environment.CurrentDirectory, nnn);
            ZapiszPlik(fs, filePath);


            Console.WriteLine("Wywoluje uploadFile()");
            filePath = Path.Combine(System.Environment.CurrentDirectory, "klient.jpg");
            UploadFile(client, "newFile.jpg", filePath);

            client.Close();
            Console.WriteLine();
            Console.WriteLine("Nacisnij dowolny przycisk zeby zakonczyc");
            Console.ReadKey();
        }

        private static void ZapiszPlik(Stream inStream, string filePath)
        {
            const int bufferLength = 8192;
            int byteCount = 0;
            int counter = 0;

            byte[] buffer = new byte[bufferLength];
            Console.WriteLine("-->Zapisuje plik {0}", filePath);
            FileStream outStream = File.Open(filePath, FileMode.Create, FileAccess.Write);

            while((counter = inStream.Read(buffer, 0, bufferLength)) > 0)
            {
                outStream.Write(buffer, 0, counter);
                Console.Write(".{0}", counter);
                byteCount += counter;
            }

            Console.WriteLine();
            Console.WriteLine("Zapisano {0} bajtów", byteCount);

            outStream.Close();
            inStream.Close();

            Console.WriteLine();
            Console.WriteLine("--> Plik {0} zapisany", filePath);
        }
        
        private static void UploadFile(StreamingServiceClient client, string fileName, string filePath)
        {
            FileStream outStream = File.Open(filePath, FileMode.Open, FileAccess.Read);
            long size = outStream.Length;
            string name = fileName;
            client.UploadFile(name, size, outStream);
        }
    }
}
