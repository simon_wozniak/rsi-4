﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StreamingClient.ServiceReference1 {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference1.IStreamingService")]
    public interface IStreamingService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IStreamingService/GetStream", ReplyAction="http://tempuri.org/IStreamingService/GetStreamResponse")]
        System.IO.Stream GetStream(string nazwa);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IStreamingService/GetStream", ReplyAction="http://tempuri.org/IStreamingService/GetStreamResponse")]
        System.Threading.Tasks.Task<System.IO.Stream> GetStreamAsync(string nazwa);
        
        // CODEGEN: Generating message contract since the wrapper name (RequestFileMessage) of message RequestFileMessage does not match the default value (GetMStream)
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IStreamingService/GetMStream", ReplyAction="http://tempuri.org/IStreamingService/GetMStreamResponse")]
        StreamingClient.ServiceReference1.ResponseFileMessage GetMStream(StreamingClient.ServiceReference1.RequestFileMessage request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IStreamingService/GetMStream", ReplyAction="http://tempuri.org/IStreamingService/GetMStreamResponse")]
        System.Threading.Tasks.Task<StreamingClient.ServiceReference1.ResponseFileMessage> GetMStreamAsync(StreamingClient.ServiceReference1.RequestFileMessage request);
        
        // CODEGEN: Generating message contract since the wrapper name (FileUploadMessage) of message FileUploadMessage does not match the default value (UploadFile)
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IStreamingService/UploadFile", ReplyAction="http://tempuri.org/IStreamingService/UploadFileResponse")]
        StreamingClient.ServiceReference1.FileUploadResponse UploadFile(StreamingClient.ServiceReference1.FileUploadMessage request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IStreamingService/UploadFile", ReplyAction="http://tempuri.org/IStreamingService/UploadFileResponse")]
        System.Threading.Tasks.Task<StreamingClient.ServiceReference1.FileUploadResponse> UploadFileAsync(StreamingClient.ServiceReference1.FileUploadMessage request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="RequestFileMessage", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class RequestFileMessage {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public string nazwa;
        
        public RequestFileMessage() {
        }
        
        public RequestFileMessage(string nazwa) {
            this.nazwa = nazwa;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="ResponseFileMessage", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class ResponseFileMessage {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://tempuri.org/")]
        public string nazwa;
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://tempuri.org/")]
        public long rozmiar;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public System.IO.Stream dane;
        
        public ResponseFileMessage() {
        }
        
        public ResponseFileMessage(string nazwa, long rozmiar, System.IO.Stream dane) {
            this.nazwa = nazwa;
            this.rozmiar = rozmiar;
            this.dane = dane;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="FileUploadMessage", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class FileUploadMessage {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://tempuri.org/")]
        public string name;
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://tempuri.org/")]
        public long size;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public System.IO.Stream stream;
        
        public FileUploadMessage() {
        }
        
        public FileUploadMessage(string name, long size, System.IO.Stream stream) {
            this.name = name;
            this.size = size;
            this.stream = stream;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="FileUploadResponse", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class FileUploadResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public bool operationSuccess;
        
        public FileUploadResponse() {
        }
        
        public FileUploadResponse(bool operationSuccess) {
            this.operationSuccess = operationSuccess;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IStreamingServiceChannel : StreamingClient.ServiceReference1.IStreamingService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class StreamingServiceClient : System.ServiceModel.ClientBase<StreamingClient.ServiceReference1.IStreamingService>, StreamingClient.ServiceReference1.IStreamingService {
        
        public StreamingServiceClient() {
        }
        
        public StreamingServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public StreamingServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public StreamingServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public StreamingServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.IO.Stream GetStream(string nazwa) {
            return base.Channel.GetStream(nazwa);
        }
        
        public System.Threading.Tasks.Task<System.IO.Stream> GetStreamAsync(string nazwa) {
            return base.Channel.GetStreamAsync(nazwa);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        StreamingClient.ServiceReference1.ResponseFileMessage StreamingClient.ServiceReference1.IStreamingService.GetMStream(StreamingClient.ServiceReference1.RequestFileMessage request) {
            return base.Channel.GetMStream(request);
        }
        
        public long GetMStream(ref string nazwa, out System.IO.Stream dane) {
            StreamingClient.ServiceReference1.RequestFileMessage inValue = new StreamingClient.ServiceReference1.RequestFileMessage();
            inValue.nazwa = nazwa;
            StreamingClient.ServiceReference1.ResponseFileMessage retVal = ((StreamingClient.ServiceReference1.IStreamingService)(this)).GetMStream(inValue);
            nazwa = retVal.nazwa;
            dane = retVal.dane;
            return retVal.rozmiar;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<StreamingClient.ServiceReference1.ResponseFileMessage> StreamingClient.ServiceReference1.IStreamingService.GetMStreamAsync(StreamingClient.ServiceReference1.RequestFileMessage request) {
            return base.Channel.GetMStreamAsync(request);
        }
        
        public System.Threading.Tasks.Task<StreamingClient.ServiceReference1.ResponseFileMessage> GetMStreamAsync(string nazwa) {
            StreamingClient.ServiceReference1.RequestFileMessage inValue = new StreamingClient.ServiceReference1.RequestFileMessage();
            inValue.nazwa = nazwa;
            return ((StreamingClient.ServiceReference1.IStreamingService)(this)).GetMStreamAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        StreamingClient.ServiceReference1.FileUploadResponse StreamingClient.ServiceReference1.IStreamingService.UploadFile(StreamingClient.ServiceReference1.FileUploadMessage request) {
            return base.Channel.UploadFile(request);
        }
        
        public bool UploadFile(string name, long size, System.IO.Stream stream) {
            StreamingClient.ServiceReference1.FileUploadMessage inValue = new StreamingClient.ServiceReference1.FileUploadMessage();
            inValue.name = name;
            inValue.size = size;
            inValue.stream = stream;
            StreamingClient.ServiceReference1.FileUploadResponse retVal = ((StreamingClient.ServiceReference1.IStreamingService)(this)).UploadFile(inValue);
            return retVal.operationSuccess;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<StreamingClient.ServiceReference1.FileUploadResponse> StreamingClient.ServiceReference1.IStreamingService.UploadFileAsync(StreamingClient.ServiceReference1.FileUploadMessage request) {
            return base.Channel.UploadFileAsync(request);
        }
        
        public System.Threading.Tasks.Task<StreamingClient.ServiceReference1.FileUploadResponse> UploadFileAsync(string name, long size, System.IO.Stream stream) {
            StreamingClient.ServiceReference1.FileUploadMessage inValue = new StreamingClient.ServiceReference1.FileUploadMessage();
            inValue.name = name;
            inValue.size = size;
            inValue.stream = stream;
            return ((StreamingClient.ServiceReference1.IStreamingService)(this)).UploadFileAsync(inValue);
        }
    }
}
