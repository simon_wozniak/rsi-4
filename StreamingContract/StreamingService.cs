﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace StreamingContract
{
    public class StreamingService : IStreamingService
    {
        public Stream GetStream(string nazwa)
        {
            FileStream myFile;
            Console.WriteLine("-->Wywolano getStream");
            string filePath = Path.Combine(System.Environment.CurrentDirectory, ".\\image.jpg");
            try
            {
                myFile = File.OpenRead(filePath);
            } catch (IOException e)
            {
                Console.WriteLine("Wyjatek otwarcia pliku {0}: ", filePath);
                Console.WriteLine(e.ToString());
                throw e;
            }
            return myFile;
        }
        public ResponseFileMessage GetMStream(RequestFileMessage request)
        {
            ResponseFileMessage response = new ResponseFileMessage();
            string nazwa = request.nazwa;
            FileStream myFile;
            Console.WriteLine("-->Wywolano getMStream");
            string filePath = Path.Combine(System.Environment.CurrentDirectory, nazwa);
            try
            {
                myFile = File.OpenRead(filePath);
                response.nazwa = "response" + nazwa;
                response.rozmiar = myFile.Length;
                response.dane = myFile;
            }
            catch (IOException e)
            {
                Console.WriteLine("Wyjatek otwarcia pliku {0}: ", filePath);
                Console.WriteLine(e.ToString());
                throw e;
            }
            return response;
        }

        public FileUploadResponse UploadFile(FileUploadMessage fileUploadMessage)
        {
            System.IO.Stream inStream = fileUploadMessage.stream;
            string filePath = Path.Combine(System.Environment.CurrentDirectory, fileUploadMessage.name);
            bool success = SaveFile(inStream, filePath);
            FileUploadResponse response = new FileUploadResponse();
            response.operationSuccess = success;
            return response;
        }

        public bool SaveFile(System.IO.Stream inStream, string filePath)
        {
            try
            {
                const int bufferLength = 8192;
                int byteCount = 0;
                int counter = 0;

                byte[] buffer = new byte[bufferLength];
                Console.WriteLine("-->Zapisuje plik {0}", filePath);
                FileStream outStream = File.Open(filePath, FileMode.Create, FileAccess.Write);

                while ((counter = inStream.Read(buffer, 0, bufferLength)) > 0)
                {
                    outStream.Write(buffer, 0, counter);
                    Console.Write(".{0}", counter);
                    byteCount += counter;
                }

                Console.WriteLine();
                Console.WriteLine("Zapisano {0} bajtów", byteCount);

                outStream.Close();
                inStream.Close();

                Console.WriteLine();
                Console.WriteLine("--> Plik {0} zapisany", filePath);
                return true;
            }
            catch(IOException e)
            {
                Console.WriteLine("File save failed");
                return false;
            }
        }
    }
}
