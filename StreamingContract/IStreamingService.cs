﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace StreamingContract
{
    [ServiceContract]
    public interface IStreamingService
    {
        [OperationContract]
        System.IO.Stream GetStream(string nazwa);

        [OperationContract]
        ResponseFileMessage GetMStream(RequestFileMessage request);

        [OperationContract]
        FileUploadResponse UploadFile(FileUploadMessage fileUploadMessage);
    }

    [MessageContract]
    public class RequestFileMessage
    {
        [MessageBodyMember]
        public string nazwa;
    }

    [MessageContract]
    public class ResponseFileMessage
    {
        [MessageHeader]
        public string nazwa;
        [MessageHeader]
        public long rozmiar;
        [MessageBodyMember]
        public System.IO.Stream dane;
    }

    [MessageContract]
    public class FileUploadMessage
    {
        [MessageHeader]
        public string name;
        [MessageHeader]
        public long size;
        [MessageBodyMember]
        public System.IO.Stream stream;
    }

    [MessageContract]
    public class FileUploadResponse
    {
        [MessageBodyMember]
        public bool operationSuccess;
    }
}
