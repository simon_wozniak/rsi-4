﻿using System;
using System.IO;
using System.ServiceModel;
using System.Windows;
using System.Windows.Controls;
using GuiClient.ServiceReference1;
using GuiClient.ServiceReference2;
using Microsoft.Win32;
using System.Threading;

namespace GuiClient
{
    public partial class MainWindow : Window
    {
        private StreamingServiceClient client = new StreamingServiceClient();
        private CallbackCalculatorClient client2;
        private TextBox uploadTextBox;
        private TextBox silniaTextBox;
        private Label silniaLabel;
        private string silniaResultText = "Silnia wynik: {0}";
        string path = Path.Combine(System.Environment.CurrentDirectory, "gt.zip");
        private ProgressBar downloadBar;
        public MainWindow()
        {
            InitializeComponent();
            CallbackCalculatorHandler handler = new CallbackCalculatorHandler(this);
            InstanceContext instanceContext = new InstanceContext(handler);
            client2 = new CallbackCalculatorClient(instanceContext);
            uploadTextBox = (TextBox)FindName("UploadTextTextBox");
            silniaTextBox = (TextBox)FindName("textBoxSilnia");
            silniaLabel = (Label)FindName("labelSilnia");
            downloadBar = (ProgressBar)FindName("downloadProgressBar");
        }

        private void OnUploadButtonClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "JPEG Files (*.jpg)|*.jpg|PNG Files (*.png)|*.png|BMP Files (*.bmp)|*.bmp|All files (*.*)|*.*";
            dialog.Multiselect = false;
            Nullable<bool> result = dialog.ShowDialog();
            if(result == true)
            {
                UploadFile(dialog.SafeFileName, dialog.FileName);
            }
        }

        private void UploadFile(string fileName, string filePath)
        {
            FileStream outStream = File.Open(filePath, FileMode.Open, FileAccess.Read);
            long size = outStream.Length;
            string name = fileName;
            client.UploadFile(name, size, outStream);
        }

        private void UploadText(object sender, RoutedEventArgs e)
        {
            client2.PrzeslijTekst(uploadTextBox.Text);
        }

        private void CalculateSilnia(object sender, RoutedEventArgs e)
        {
            int silniaInput = int.Parse(silniaTextBox.Text);
            client2.Silnia(silniaInput);
        }

        public void ResultSilnia(double result)
        {
            silniaLabel.Content = string.Format(silniaResultText, result);
        }

        public void ObliczCosResult(string result)
        {

        }

        public void ZapiszPlik(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Wywoluje getMStream()");
            Stream fs = null;
            long rozmiar;
            string nnn = "gt.zip";
            rozmiar = client.GetMStream(ref nnn, out fs);
            new Thread(() =>
            {
                ZapiszPlik(fs, rozmiar);
            }).Start();
        }

        private void ZapiszPlik(Stream inStream, long fullSize)
        {
            const int bufferLength = 8192;
            int byteCount = 0;
            int counter = 0;

            byte[] buffer = new byte[bufferLength];
            Console.WriteLine("-->Zapisuje plik {0}", path);
            FileStream outStream = File.Open(path, FileMode.Create, FileAccess.Write);

            while ((counter = inStream.Read(buffer, 0, bufferLength)) > 0)
            {
                outStream.Write(buffer, 0, counter);
                Console.Write(".{0}", counter);
                byteCount += counter;
                Dispatcher.Invoke((() =>
                {
                    downloadBar.Value = (byteCount / (double)fullSize) * 100;
                }));
            }

            Console.WriteLine();
            Console.WriteLine("Zapisano {0} bajtów", byteCount);

            outStream.Close();
            inStream.Close();

            Console.WriteLine();
            Console.WriteLine("--> Plik {0} zapisany", path);
        }
    }
}
