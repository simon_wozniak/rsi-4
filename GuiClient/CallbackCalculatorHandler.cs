﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuiClient.ServiceReference2;
namespace GuiClient
{
    class CallbackCalculatorHandler : ICallbackCalculatorCallback
    {
        MainWindow window;

        public CallbackCalculatorHandler(MainWindow window)
        {
            this.window = window;
        }

        public void ZwrotObliczCos(string result)
        {
            window.ObliczCosResult(result);
        }

        public void ZwrotSilnia(double result)
        {
            window.ResultSilnia(result);
        }
    }
}
